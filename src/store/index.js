import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// const NewsAPI = require('newsapi')
// const newsapi = new NewsAPI('760b448a87c34b9babdc6b50e9a700b4')
const url = 'http://localhost:8080/v2/sources?apiKey=760b448a87c34b9babdc6b50e9a700b4'
const searchUrl = 'http://localhost:8080/v2/top-headlines?apiKey=760b448a87c34b9babdc6b50e9a700b4'

export default new Vuex.Store({
  state: {
    newsData: [],
    loadingStatus: false,
    newsLoadingStatus: false,
    categoryData: [],
    countryData: []
  },
  mutations: {
    setSearchNews (state, payload) {
      state.newsData.push(...payload)
    },
    setCategories (state, payload) {
      state.categoryData = payload
    },
    setCountries (state, payload) {
      state.countryData = payload
    },
    loadingStatus (state, newLoadingStatus) {
      state.loadingStatus = newLoadingStatus
    },
    newsLoadingStatus (state, newLoadingStatus) {
      state.newsLoadingStatus = newLoadingStatus
    },
    clearNews (state) {
      state.newsData = []
    }
  },
  actions: {
    async setCurrentData (state, payload) {
      state.commit('loadingStatus', true)
      const DATA = await fetch(url)
      const N = await DATA.json()
      const categories = await N.sources.filter((v, i, a) => a.findIndex(t => (t.category === v.category)) === i)
        .map(function (obj) {
          return obj.category
        })
      const countries = await N.sources.filter((v, i, a) => a.findIndex(t => (t.country === v.country && t.language !== 'zh')) === i)
        .map(function (obj) {
          return obj.country
        })
      state.commit(`setCategories`, categories)
      state.commit(`setCountries`, countries)
      state.commit('loadingStatus', false)
    },
    async searchForNews (state, commit) {
      if (commit.buttonClicked) state.commit('clearNews')
      state.commit('newsLoadingStatus', true)
      const NEWS = await fetch(
        searchUrl + `&country=${encodeURIComponent(commit.country)}&category=${encodeURIComponent(commit.category)}&q=${encodeURIComponent(commit.keyword)}&page=${encodeURIComponent(commit.page)}&pageSize=${encodeURIComponent(commit.pageSize)}`
      )
      const N = await NEWS.json()
      console.log(N)
      state.commit(`setSearchNews`, N.articles)
      state.commit('newsLoadingStatus', false)
    }
  },
  modules: {},
  getters: {
    getNews: state => state.newsData,
    getCountries: state => state.countryData,
    getCategories: state => state.categoryData,
    getLoadingState: state => state.loadingStatus,
    getNewsLoadingState: state => state.newsLoadingStatus
  }
})
